## Info
Git ini adalah hasil pembuatan dari laravel dengan multiple auth admin dan user

Tutorial ini berdasarkan pada video playlist weeework : https://www.youtube.com/playlist?list=PL1aMeb5UP_PE_IA_66haIf_m6rwWyWU_d

## Cara Install
```
git clone git@gitlab.com:juniyadi/laravel-multiauth.git
cd laravel-multiauth
composer install
php artisan cache:clear
cp -r .env.example .env
```

## Generate Key & Setup database
----
rubah file .env pada line database, user database dan password kemudian untuk generate database jalankan command
```
php artisan key:generate
php artisan migrate
```

## Cara Membuat Akun Admin dengan tinker
```
./artisan tinker
$admin = new App\Admin();
$admin->name = 'Administrator'
$admin->email = 'administrator@mail.com'
$admin->password = bcrypt('password')
$admin->save();
```

## Login URL
```
Admin = http://localhost/admin/login
User = http://localhost/login
```

## Info tambahan
----
Jika sudah masuk ke tahap produksi, pastikan anda menonaktifkan debug laravel, caranya dengan edit file .env kemudian rubah menjadi seperti ini :
```
APP_DEBUG=false
```

----
Jika anda belum mengaktifkan email service, rubah pada baris .env kembali dan edit menjadi seperti ini :
```
MAIL_DRIVER=log
```

Semua log (termasuk reset link password) akan tersimpan pada /storage/log/laravel.log