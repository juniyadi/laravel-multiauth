@if(Auth::guard('web')->check())
	<p class="text-success">
		Kamu login sebagai <strong>USER</strong>
	</p>
@else
	<p class="text-danger">
		Kamu logout sebagai <strong>USER</strong>
	</p>
@endif

@if(Auth::guard('admin')->check())
	<p class="text-success">
		Kamu login sebagai <strong>ADMIN</strong>
	</p>
@else
	<p class="text-danger">
		Kamu logout sebagai <strong>ADMIN</strong>
	</p>
@endif